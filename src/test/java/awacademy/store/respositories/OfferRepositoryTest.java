package awacademy.store.respositories;

import awacademy.store.entities.OfferEntity;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(
        properties = {
                "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class OfferRepositoryTest {

    @Autowired
    private OfferRepository repositoryUnderTest;


    @Test
    void itShouldFindOrderByCode() {

        // Given
        OfferEntity offer = new OfferEntity("Test","199","JunitTesting");
        String code = UUID.randomUUID().toString()+ "O";
        offer.setCode(code);

        // When
        repositoryUnderTest.save(offer);

        // Then
        assertThat(repositoryUnderTest.findOrderByCode(code))
                .isPresent()
                .hasValueSatisfying(offerEntity -> assertThat(offerEntity)
                .isEqualToComparingFieldByField(offer));
    }
}
