package awacademy.store.respositories;

import awacademy.store.entities.BusinessEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import static org.assertj.core.api.Assertions.assertThat;


/**
 * In order to test against a H2 database I have to use @DataJpaTest annotation
 */

@DataJpaTest(
properties = {
        "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class BusinessRepositoryTest {

    /**
     * shift strg(cmd) + t -> shortcut coming back to the class/interface which is tested
     */


    @Autowired
    private BusinessRepository underTest;


    /**
     * Testing my custom query in BusinessRepository
     */
    @Test
    void itShouldFindBusinessByTimeStamp() {

        //Given
        // e.g. date of business registration
        Date registeredTime = java.sql.Date.valueOf("1993-07-23");
        Date registeredTime2 = java.sql.Date.valueOf("1993-08-10");


        // simulated timestamps client sends
        Date timeStamp1 = java.sql.Date.valueOf("1993-01-30");
        Date timeStamp2 = java.sql.Date.valueOf("1993-12-31");

        BusinessEntity business = new BusinessEntity("TestBusiness",
                new Timestamp(registeredTime.getTime()), true);
        BusinessEntity business2 = new BusinessEntity("TestBusiness2",
                new Timestamp(registeredTime2.getTime()), false);

        underTest.save(business);
        underTest.save(business2);

        // When

        List<Optional<BusinessEntity>> listOfBusinesses = underTest.findByTimeStamp(timeStamp1,timeStamp2);

        // Then

        assertThat(listOfBusinesses)
                .isNotEmpty()
                .hasSize(2);


        assertThat(listOfBusinesses.get(0))
                .hasValueSatisfying(businessEntity -> assertThat(businessEntity)
                        .isEqualToComparingFieldByField(business));

        assertThat(listOfBusinesses.get(1))
                .hasValueSatisfying(businessEntity -> assertThat(businessEntity)
                        .isEqualToComparingFieldByField(business2));

    }
}
