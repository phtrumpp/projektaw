package awacademy.store.respositories;

import awacademy.store.entities.BusinessEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;


@Repository
public interface BusinessRepository extends JpaRepository<BusinessEntity, Long> {



    /*

    @Param ist nützlich bei Queries mit mehreren Parameter Anfragen, da im Normalfall
    Der erste Parameter PLatzhalter mit dem ersten Query ersetzt wird.
    Mit Param definiert man den konkreten Parameter Namen auch für die Query

     */

    /**
     * Admin Query for REST API
     * @param firstTimeStamp Time Period
     * @param secondTimeStamp Time Period
     * @return List of businesses for the given time period
     */
    @Query("select b from BusinessEntity b where b.timeStamp between :firstTimeStamp and :secondTimeStamp")
    List<Optional<BusinessEntity>> findByTimeStamp(@Param("firstTimeStamp") Date firstTimeStamp, @Param("secondTimeStamp") Date secondTimeStamp);



}
