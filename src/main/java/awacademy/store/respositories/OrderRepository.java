package awacademy.store.respositories;


import awacademy.store.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

// Annotation is needed to declare this class as a spring bean
@Repository
public interface OrderRepository extends JpaRepository<OrderEntity,Long> {
    OrderEntity findByDateOfOrder (String date);



}
