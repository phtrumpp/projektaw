package awacademy.store.respositories;


import awacademy.store.entities.OfferEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OfferRepository extends JpaRepository<OfferEntity,Long> {



    @Query("select o from OfferEntity o where o.code = :code")
    Optional<OfferEntity> findOrderByCode(@Param("code") String code);



}
