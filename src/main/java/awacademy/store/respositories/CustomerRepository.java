package awacademy.store.respositories;

import awacademy.store.entities.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity,Long> {
    // Todo - Noch in Absprache mit dem External Service - Noch nicht geklärt ob die Customer Entität überhaupt nötig ist
}
