package awacademy.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class StoreApplication {


    /**
     * Definieren uns eine eigene Bean, damit alle Klassen diese im Falle injecten können
     *
     * @return Resttemplate Instanz
     */
    @Bean
    public RestTemplate createRestTemplate(){
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }

}
