package awacademy.store.services;

import awacademy.store.dtos.AdminDTO;
import awacademy.store.entities.BusinessEntity;
import awacademy.store.respositories.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AdminService {


    private final BusinessRepository businessRepository;

    @Autowired
    public AdminService(BusinessRepository companyRepository) {
        this.businessRepository = companyRepository;
    }


    /**
     * Modul Verwaltung wünscht sich eine Liste von Businesses mit Zeitstempel
     * @return list of businesses
     */
    public List<AdminDTO> readAllCompanies(Date startDate, Date endDate) {

        List<Optional<BusinessEntity>> businessEntityList = businessRepository.findByTimeStamp(startDate,endDate);

        // controls if list returned something or is empty, and flatmaps it - more code but better control?
        List<BusinessEntity> flatMappedList = businessEntityList.stream()
                .flatMap(optional -> optional.isPresent() ? Stream.of(optional.get()): Stream.empty())
                .collect(Collectors.toList());


        return flatMappedList.stream()
                .map(businessEntity -> new AdminDTO(
                        businessEntity.getBusinessName(),
                        businessEntity.getCity(),
                        businessEntity.getEmail(),
                        businessEntity.getTimeStamp(),
                        businessEntity.getId()))
                .collect(Collectors.toList());


    }

}

