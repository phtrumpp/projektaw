package awacademy.store.services;

import awacademy.store.dtos.BusinessIdDTO;

import awacademy.store.dtos.OfferToSendDTO;
import awacademy.store.dtos.ess.OcodeDTO;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class ExternalServiceService {

    private final RestTemplate restTemplate;
    private final String URL = "https://linkogservices.herokuapp.com/business/registration";
    private final String URL2 = "https://linkogservices.herokuapp.com/business/offer";

    @Autowired
    public ExternalServiceService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    //There is a good reason why we use a Long id instead of the whole DTO object within the id
    public String businessRegistration(Long id) {

        // We create a DTO so we can send it as a JSON object to the ESS
        BusinessIdDTO businessIdDTO = new BusinessIdDTO(id);

        //RestTemplate aufrufen und code als String zurückbekommen

        return restTemplate.postForObject(URL, businessIdDTO, String.class);
    }


    public OfferToSendDTO offerRegistration(OfferToSendDTO offerToSendDTO) {

        String code = UUID.randomUUID().toString() + "O";

        OcodeDTO ocodeDTO = new OcodeDTO();

        ocodeDTO.setCode(code);
        ocodeDTO.setId(offerToSendDTO.getBusinessId());

        // if needed in log
        String response = restTemplate.postForObject(URL2, ocodeDTO, String.class);
        System.out.println(response);

        offerToSendDTO.setCode(code);

        return offerToSendDTO;
    }


}
