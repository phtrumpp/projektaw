package awacademy.store.services;

import awacademy.store.dtos.OrderDt;
import awacademy.store.dtos.ess.EssOrderDTO;
import awacademy.store.entities.OfferEntity;
import awacademy.store.entities.OrderEntity;
import awacademy.store.respositories.OfferRepository;
import awacademy.store.respositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class OrderService {


    private final OrderRepository orderRepository;
    private final OfferRepository offerRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, OfferRepository offerRepository) {
        this.orderRepository = orderRepository;
        this.offerRepository = offerRepository;
    }

    /**
     * This takes the response Code from ESS and looks for the Offer in DB, then it
     * maps all the needed info of the offer (title, description) and saves it as an
     * order to the DB. Later we can simply read our orders from DB.
     * @param essOrderDTO -
     */
    public void createOrder(EssOrderDTO essOrderDTO) {

        // we get a DateTime
        LocalDateTime dateTime = LocalDateTime.now();
        String orderDate = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        // Reads the required Offer (from api response code)
        Optional<OfferEntity> offerEntity = offerRepository.findOrderByCode(essOrderDTO.getCode());

        // maps it to order entity and saves it in DB
        OrderEntity newOrder = offerEntity
                .map(offer -> new OrderEntity(
                        offer.getCustomerID(),
                        offer.getTitle(),
                        offer.getAbout(),
                        orderDate))
                .orElseThrow(() -> new IllegalStateException("Could not create order"));

        orderRepository.save(newOrder);

    }

    public List<OrderDt> readAllOrders() {

        List<OrderEntity> orderEntities = orderRepository.findAll();

        return orderEntities.stream()
                .map(orderEntity -> new OrderDt(
                        orderEntity.getTitle(),
                        orderEntity.getDescription(),
                        orderEntity.getDateOfOrder(),
                        orderEntity.getId(),
                        orderEntity.getCustomerID()))
                .collect(Collectors.toList());
    }


//    private OrderEntity mapToOrder(OfferEntity offerEntity) {
//
//        OrderEntity newOrder = new OrderEntity();
//        newOrder.setDescription(offerEntity.getPrice());
//        newOrder.setTitle(offerEntity.getTitle());
//        newOrder.setId(offerEntity.getId());
//        newOrder.setCustomerID(offerEntity.getCustomerID());
//
//        // we get a DateTime
//        LocalDateTime dateTime = LocalDateTime.now();
//        String orderDate = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//        newOrder.setDateOfOrder(orderDate);
//
//        return newOrder;
//
//    }


}
