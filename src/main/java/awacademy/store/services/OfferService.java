package awacademy.store.services;


import awacademy.store.dtos.OfferDTO;
import awacademy.store.dtos.OfferToSendDTO;
import awacademy.store.entities.OfferEntity;
import awacademy.store.respositories.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class OfferService {


    private final OfferRepository offerRepository;

    @Autowired
    public OfferService(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }


    public void createOffer(OfferToSendDTO offerToSendDTO) {

        OfferEntity offerEntity = Optional.of(offerToSendDTO)
                .map(offerDTO -> new OfferEntity(
                        offerDTO.getTitle(),
                        offerDTO.getPrice(),
                        offerDTO.getAbout()))
                .orElseThrow(() -> new IllegalStateException("Missing OfferDTO"));

        offerRepository.save(offerEntity);

    }

    public List<OfferDTO> readAllOffer() {

        List<OfferDTO> offerDTOList = offerRepository.findAll()
                .stream()
                .map(offer -> new OfferDTO(
                        offer.getTitle(),
                        offer.getPrice(),
                        offer.getAbout(),
                        offer.getCode(),
                        offer.getId()))
                .collect(Collectors.toList());

        return Optional.of(offerDTOList)
                .orElseThrow(() -> new IllegalStateException("No offer available"));

    }


    public void deleteOffer(OfferDTO offerDTO) {

        OfferEntity offer = offerRepository.findById(offerDTO.getId())
                .orElseThrow(() -> new NullPointerException("Offer not in database"));

        offerRepository.delete(offer);
    }


    // readOnly makes sure this method can't override and persist but only read the required value
    @Transactional(readOnly = true)
    public OfferEntity findById(Long id) {

        return offerRepository.findAll().stream()
                .filter(offerEntity -> offerEntity.getId().equals(id))
                .findFirst()
                .orElseThrow(()-> new IllegalStateException("No user with this id found"));

//        return offerRepository.findById(id)
//                .orElseThrow(() -> new IllegalStateException("No user with this id found"));

    }


    public OfferEntity updateEntireOffer(OfferDTO offerDTO) {

        OfferEntity updatedOffer = offerRepository.findById(offerDTO.getId())
                .orElseThrow(()-> new IllegalStateException("Nothing to update"));

        updatedOffer.setAbout(offerDTO.getAbout());
        updatedOffer.setPrice(offerDTO.getPrice());
        updatedOffer.setTitle(offerDTO.getTitle());

        offerRepository.save(updatedOffer);

        return updatedOffer;
    }


}
