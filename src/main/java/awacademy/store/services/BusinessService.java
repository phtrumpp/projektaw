package awacademy.store.services;

import awacademy.store.dtos.BusinessDTO;
import awacademy.store.entities.BusinessEntity;
import awacademy.store.respositories.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusinessService {


    private final BusinessRepository businessRepository;

    @Autowired
    public BusinessService(BusinessRepository businessRepository) {
        this.businessRepository = businessRepository;
    }



    public List<BusinessDTO> readAllBusiness() {

        List<BusinessDTO> businessDTOList = new ArrayList<>();
        List<BusinessEntity> businessEntityList =  businessRepository.findAll();

        for (BusinessEntity businessEntity : businessEntityList) {
            businessDTOList.add(ModelMapper(businessEntity));
        }

        return businessDTOList;
    }

    public void createNewBusiness(BusinessDTO businessDto){

        // Die Frage ist, wird hier bereits eine Business ID generiert?
        BusinessEntity businessEntity = EntityMapper(businessDto);

        //Timestamp - mainly needed for AdminModule
        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(localDateTime);
        businessEntity.setTimeStamp(timestamp);

        businessRepository.save(businessEntity);


    }

    public BusinessEntity EntityMapper(BusinessDTO businessDto){

        BusinessEntity businessEntity = new BusinessEntity();

        businessEntity.setBusinessName(businessDto.getBusinessName());
        businessEntity.setPassword(businessDto.getPassword());
        businessEntity.setStreet(businessDto.getStreet());
        businessEntity.setBranche(businessDto.getBranche());
        businessEntity.setEmail(businessDto.getEmail());
        businessEntity.setPhoneNumber(businessDto.getPhoneNumber());
        businessEntity.setCity(businessDto.getCity());
        businessEntity.setCountry(businessDto.getCountry());
        businessEntity.setPassword(businessDto.getPassword());
        businessEntity.setPostcode(businessDto.getPostcode());
        businessEntity.setOnlinePayment(businessDto.isOnlinePayment());
        //mappen of qrCode
        businessEntity.setQrCode(businessDto.getQrCode());

        return businessEntity;
    }

    public BusinessDTO ModelMapper(BusinessEntity businessEntity) {

        BusinessDTO businessDto = new BusinessDTO();
        businessDto.setStreet(businessEntity.getStreet());
        businessDto.setCity(businessEntity.getCity());
        businessDto.setCountry(businessEntity.getCountry());
        businessDto.setPhoneNumber(businessEntity.getPhoneNumber());
        businessDto.setPostcode(businessEntity.getPostcode());
        businessDto.setStreet(businessEntity.getStreet());
        businessDto.setOnlinePayment(businessEntity.isOnlinePayment());
        businessDto.setBranche(businessEntity.getBranche());
        businessDto.setEmail(businessEntity.getEmail());
        businessDto.setBusinessName(businessEntity.getBusinessName());
        //BusinessModel erhält ID aus DB
        //ToDo - This might not be up to Date!!
        businessDto.setId(businessEntity.getId());
        businessDto.setQrCode(businessEntity.getQrCode());

        return businessDto;

    }




    @Transactional(readOnly = true)
    public BusinessEntity findById(Long id) {
        return businessRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException("No business found"));
    }



    public void updateBusinessInfo(BusinessDTO businessDTO){

        BusinessEntity updatedBusinessInfo = findById(businessDTO.getId());

        updatedBusinessInfo.setBusinessName(businessDTO.getBusinessName());
        updatedBusinessInfo.setCity(businessDTO.getCity());
        updatedBusinessInfo.setCountry(businessDTO.getCountry());
        updatedBusinessInfo.setBranche(businessDTO.getBranche());
        updatedBusinessInfo.setEmail(businessDTO.getEmail());
        updatedBusinessInfo.setOnlinePayment(businessDTO.isOnlinePayment());
        updatedBusinessInfo.setPhoneNumber(businessDTO.getPhoneNumber());

        businessRepository.save(updatedBusinessInfo);

    }


}
