package awacademy.store.entities;

import lombok.Data;

import javax.persistence.*;


/**
 * In relation to external service
 */
@Entity
@Data
@Table(name = "order")
public class OrderEntity {


    public OrderEntity() {
    }

    public OrderEntity(Long customerID,String title, String description,
                       String dateOfOrder){
        this.customerID = customerID;
        this.title = title;
        this.description = description;
        this.dateOfOrder = dateOfOrder;
    }

    // We get from ESS
    private Long customerID;


    private String title;

    private String description;

    private String dateOfOrder;



    @Id
    @GeneratedValue
    private Long id;

    // generated code by ESS
    private String code;




}
