package awacademy.store.exceptions;


/**
 * Exception for our Status Codes and Errors when using REST APIs
 */
public class BusinessNotFoundException extends RuntimeException {
    public BusinessNotFoundException(String message) {
        super(message);
    }
}
