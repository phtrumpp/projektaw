package awacademy.store.Controller;


import awacademy.store.dtos.OrderDt;
import awacademy.store.dtos.ess.EssOrderDTO;
import awacademy.store.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }


    @PostMapping("/createOrder")
    public String postOrder(@RequestBody EssOrderDTO essOrderDTO){

        orderService.createOrder(essOrderDTO);

        return "Order success";

    }

    /**
     * Everytime my Angular Service renders the list of Orders this API will get called
     * @return list of order
     */
    @GetMapping("/getOrder")
    public List<OrderDt> readAllOrders(){
        return orderService.readAllOrders();
    }
}
