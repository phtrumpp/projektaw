package awacademy.store.Controller;


import awacademy.store.dtos.OfferDTO;
import awacademy.store.dtos.OfferToSendDTO;
import awacademy.store.services.ExternalServiceService;
import awacademy.store.services.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/offer") //creates url
public class OfferController {


    private final OfferService offerService;
    private final ExternalServiceService externalServiceService;

    @Autowired
    public OfferController(OfferService offerService, ExternalServiceService externalServiceService) {
        this.offerService = offerService;
        this.externalServiceService = externalServiceService;
    }

    @PostMapping("/postoffer")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOffer(@RequestBody OfferToSendDTO offerToSendDTO) {
        // @RequestBody maps our JSON object to an OfferDTO

        OfferToSendDTO offerTosafe = externalServiceService.offerRegistration(offerToSendDTO);
        offerService.createOffer(offerTosafe);
    }

    @GetMapping("/getalloffers")
    public List<OfferDTO> readAll() {
        return offerService.readAllOffer();
    }


    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestBody OfferDTO offerDTO) {
        offerService.deleteOffer(offerDTO);
    }

}
