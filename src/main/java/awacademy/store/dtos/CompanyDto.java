package awacademy.store.dtos;

import lombok.Data;

@Data
public class CompanyDto {
    private String name;
    private String password;
    private String address;
    private String phoneNumber;
    private String mail;
    private String category;
    private boolean onlinePayment = false;
    private Long id;

    public CompanyDto(String name, String password, String address,
                      String phoneNumber, String mail, String category,
                      boolean onlinePayment, Long id) {
        this.name = name;
        this.password = password;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.mail = mail;
        this.category = category;
        this.onlinePayment = onlinePayment;
        this.id = id;
    }
}
