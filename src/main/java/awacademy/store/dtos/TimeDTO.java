package awacademy.store.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class TimeDTO {

    private Date startDate;
    private Date endDate;

    public TimeDTO(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
