package awacademy.store.dtos;


import lombok.Data;



@Data
public class OrderDt {


    private String title;
    private String description;
    private String dateOfOrder;

    // Stays empty until mapping(!) and can be used as a orderNumber for the frontend or whatever
    private Long id;

    private Long customerID;

    public OrderDt(String title, String description,
                   String dateOfOrder, Long id, Long customerID) {
        this.title = title;
        this.description = description;
        this.dateOfOrder = dateOfOrder;
        this.id = id;
        this.customerID = customerID;
    }
}
