package awacademy.store.dtos;

import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
public class AdminDTO {

    private String businessName;
    private String city;
    private String country;
    private String email;
    private Timestamp timeStamp;

    private Long id;

    public AdminDTO() {
    }

    public AdminDTO(String businessName, String city,
                    String email, Timestamp timeStamp, Long id) {
        this.businessName = businessName;
        this.city = city;
        this.email = email;
        this.timeStamp = timeStamp;
        this.id = id;
    }
}
