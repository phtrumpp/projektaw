package awacademy.store.dtos;

import lombok.Data;


@Data
public class BusinessDTO {

    private String businessName;
    private String password;
    private String street;
    private String postcode;
    private String city;
    private String country;
    private String phoneNumber;
    // email = username in auth
    private String email;
    private String branche;
    private boolean onlinePayment;


    private String qrCode;
    private Long id;

    public BusinessDTO() {
    }

    public BusinessDTO(String businessName, String password, String street,
                       String postcode, String city, String country, String phoneNumber,
                       String email, String branche, boolean onlinePayment,
                       String qrCode, Long id) {
        this.businessName = businessName;
        this.password = password;
        this.street = street;
        this.postcode = postcode;
        this.city = city;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.branche = branche;
        this.onlinePayment = onlinePayment;
        this.qrCode = qrCode;
        this.id = id;
    }
}
