package awacademy.store.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


@Data
public class OfferDTO {

    private String title;
    private String price;
    private String about;



    // Generierter QR-String-Code (Mit O)
    private String code;

    // This stays empty until mapping(!)
    @JsonIgnore
    private Long id;


    public OfferDTO(String title, String price, String about, String code, Long id) {
        this.title = title;
        this.price = price;
        this.about = about;
        this.code = code;
        this.id = id;
    }


}
